package ru.fedun.tm.api;

public interface ICommandController {

    void displayWelcome();

    void displayHelp();

    void displayVersion();

    void displayAbout();

    void displayCommands();

    void displayArgs();

    void displayInfo();

    void displayError();

    void exit();

}
