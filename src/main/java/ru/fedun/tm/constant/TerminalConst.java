package ru.fedun.tm.constant;

public interface TerminalConst {

    String HELP = "help";

    String VERSION = "version";

    String ABOUT = "about";

    String INFO = "info";

    String ARGUMENTS = "arguments";

    String COMMANDS = "commands";

    String EXIT = "exit";

}
