package ru.fedun.tm.repository;

import ru.fedun.tm.api.ICommandRepository;
import ru.fedun.tm.constant.ArgumentConst;
import ru.fedun.tm.constant.TerminalConst;
import ru.fedun.tm.model.TerminalCommand;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final TerminalCommand HELP = new TerminalCommand(
            TerminalConst.HELP, ArgumentConst.HELP, "Display list of terminal commands."
    );

    public static final TerminalCommand ABOUT = new TerminalCommand(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, "Display developer info."
    );

    public static final TerminalCommand VERSION = new TerminalCommand(
            TerminalConst.VERSION, ArgumentConst.VERSION, "Display program version."
    );

    public static final TerminalCommand INFO = new TerminalCommand(
            TerminalConst.INFO, ArgumentConst.INFO, "Display system information."
    );

    public static final TerminalCommand ARGUMENT = new TerminalCommand(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Display program arguments."
    );

    public static final TerminalCommand COMMAND = new TerminalCommand(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS, "Display program commands."
    );

    public static final TerminalCommand EXIT = new TerminalCommand(
            TerminalConst.EXIT, null, "Shutdown program."
    );

    private static final TerminalCommand[] TERMINAL_COMMANDS = new TerminalCommand[]{
            HELP, ABOUT, VERSION, INFO, COMMAND, ARGUMENT, EXIT
    };

    private final String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    private final String[] ARGS = getArgs(TERMINAL_COMMANDS);

    public String[] getCommands(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (TerminalCommand value : values) {
            final String name = value.getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (TerminalCommand value : values) {
            final String name = value.getArg();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public TerminalCommand[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    public String[] getCommands() {
        return COMMANDS;
    }

    public String[] getArgs() {
        return ARGS;
    }

}
