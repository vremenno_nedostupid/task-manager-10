package ru.fedun.tm.bootstrap;

import ru.fedun.tm.api.ICommandController;
import ru.fedun.tm.api.ICommandRepository;
import ru.fedun.tm.api.ICommandService;
import ru.fedun.tm.constant.ArgumentConst;
import ru.fedun.tm.controller.CommandController;
import ru.fedun.tm.repository.CommandRepository;
import ru.fedun.tm.service.CommandService;

import java.util.Scanner;

import static ru.fedun.tm.constant.TerminalConst.*;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(final String[] args) {
        commandController.displayWelcome();
        if (args.length != 0) {
            runWithArgs(args[0]);
        }
        runWithCommand();
    }

    private void runWithCommand() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (true) {
            command = scanner.nextLine();
            switch (command) {
                case VERSION:
                    commandController.displayVersion();
                    break;
                case ABOUT:
                    commandController.displayAbout();
                    break;
                case HELP:
                    commandController.displayHelp();
                    break;
                case INFO:
                    commandController.displayInfo();
                    break;
                case COMMANDS:
                    commandController.displayCommands();
                    break;
                case ARGUMENTS:
                    commandController.displayArgs();
                    break;
                case EXIT:
                    commandController.exit();
                default:
                    commandController.displayError();
            }
        }
    }

    private void runWithArgs(final String arg) {
        if (arg == null || arg.isEmpty()) {
            System.out.println("Empty command");
            return;
        }
        switch (arg) {
            case ArgumentConst.HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.INFO:
                commandController.displayInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.displayArgs();
                break;
            case ArgumentConst.COMMANDS:
                commandController.displayCommands();
                break;
            default:
                commandController.displayError();
        }
    }

}
